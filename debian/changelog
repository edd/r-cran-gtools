gtools (3.9.5-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 13)

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 28 Nov 2023 20:40:00 -0600

gtools (3.9.4-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Switch to virtual debhelper-compat (= 12)

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 27 Nov 2022 17:01:17 -0600

gtools (3.9.3-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 16 Jul 2022 13:50:34 -0500

gtools (3.9.2.2-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 17 Jun 2022 13:18:00 -0500

gtools (3.9.2.1-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 23 May 2022 06:40:25 -0500

gtools (3.9.2-2) unstable; urgency=medium

  * Simple rebuild for unstable following Debian 11 release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 23 May 2022 06:39:51 -0500

gtools (3.9.2-1) experimental; urgency=medium

  * New upstream release (to experimental during freeze)
  
  * debian/control: Set Build-Depends: to recent R version
  * debian/control: Switch to virtual debhelper-compat (= 11)
  * debian/compat: Removed

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 12 Jun 2021 12:17:26 -0500

gtools (3.8.2-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 08 Apr 2020 14:29:58 -0500

gtools (3.8.1-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Build-Depends: to 'debhelper (>= 10)'
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Add Vcs-Browser: and Vcs-Git:
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem
  * debian/compat: Set level to 9

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 27 Jun 2018 16:44:54 -0500

gtools (3.5.0-2) unstable; urgency=medium

  * Rebuilt under R 3.4.0 to update registration for .C() and .Fortran()
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Add Depends: on ${misc:Depends}

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 04 Jun 2017 19:33:04 -0500

gtools (3.5.0-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 30 May 2015 16:50:17 -0500

gtools (3.4.2-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 11 Apr 2015 09:08:29 -0500

gtools (3.4.1-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 28 May 2014 22:07:23 -0500

gtools (3.4.0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 17 Apr 2014 20:29:03 -0500

gtools (3.3.1-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 01 Mar 2014 19:13:37 -0600

gtools (3.3.0-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 11 Feb 2014 19:55:59 -0600

gtools (3.2.1-1) unstable; urgency=low

  * New upstream release

  * debian/control: Remove (Build-)Depends: on gdata

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 13 Jan 2014 10:41:00 -0600

gtools (3.2.0-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Add new (Build-)Depends: on gdata

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 12 Jan 2014 10:14:05 -0600

gtools (3.1.1-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 06 Nov 2013 13:43:53 -0600

gtools (3.1.0-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 23 Sep 2013 13:17:10 -0500

gtools (3.0.0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 13 Jul 2013 12:26:16 -0500

gtools (2.7.1-2) unstable; urgency=low

  * debian/control: Set Build-Depends: to current R version 
  
  * (Re-)building with R 3.0.0 (beta)

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 30 Mar 2013 17:52:30 -0500

gtools (2.7.1-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 24 Mar 2013 10:22:04 -0500

gtools (2.7.0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Change Depends to ${R:Depends}
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 20 Jun 2012 06:10:15 -0500

gtools (2.6.2-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 02 May 2010 21:14:55 -0500

gtools (2.6.1-2) unstable; urgency=low

  * Rebuilt for R 2.10.0 to work with new R-internal help file conversion

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  
 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 01 Nov 2009 16:13:38 -0600

gtools (2.6.1-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Changed Section: to new section 'gnu-r'
  
  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 10 May 2009 21:59:21 -0500

gtools (2.5.0-1-1) unstable; urgency=low

  * New upstream release

  * debian/control: (Build-)Depends: updated to r-base-(core|dev) (>= 2.8.1)
  * debian/control: Updated Standards-Version: to 3.8.0

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 19 Feb 2009 20:48:16 -0600

gtools (2.5.0-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 27 May 2008 06:30:42 -0500

gtools (2.4.0-1) unstable; urgency=low

  * New upstream release
  * debian/control: (Build-)Depends: updated to r-base-dev (>= 2.5.1)

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 12 Aug 2007 11:07:45 -0500

gtools (2.3.1-1) unstable; urgency=low

  * New upstream release
  * debian/control: (Build-)Depends: updated to r-base-dev (>> 2.4.1)

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 12 Apr 2007 23:17:35 -0500

gtools (2.3.0-1) unstable; urgency=low

  * New upstream release
  * debian/rules: Switched to one-line cdbs stanza
  * debian/control: Updated (Build-)Depends: to r-base-core (>= 2.4.0)
  * debian/control: Updated Standards-Version: to 3.7.2
  * debian/watch: Updated to version=3

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 28 Nov 2006 21:11:23 -0600

gtools (2.2.3-3) unstable; urgency=low

  * debian/control: Use Build-Depends:, not BD-Indep (Closes: #345908)

 -- Dirk Eddelbuettel <edd@debian.org>  Wed,  4 Jan 2006 06:35:48 -0600

gtools (2.2.3-2) unstable; urgency=low

  * debian/control: Switch to Architecture: any	      (Closes: #345807)
  * debian/control: Added ${shlibs:Depends} to Depends: stanza

 -- Dirk Eddelbuettel <edd@debian.org>  Tue,  3 Jan 2006 19:59:20 -0600

gtools (2.2.3-1) unstable; urgency=low

  * New upstream release
  
  * debian/rules: No longer call R to update html index

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 21 Dec 2005 18:22:09 -0600

gtools (2.2.2-1) unstable; urgency=low

  * New upstream release
  
  * debian/watch: Updated regular expression

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 14 Dec 2005 21:21:55 -0600

gtools (2.1.1-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 23 Sep 2005 23:13:54 -0500

gtools (2.1.0-2) unstable; urgency=low

  * debian/control: Added 'Replaces: r-cran-gregmisc'

 -- Dirk Eddelbuettel <edd@debian.org>  Sat,  3 Sep 2005 21:08:51 -0500

gtools (2.1.0-1) unstable; urgency=low

  * Initial Debian release  [ but this package used to be part of the 
    gregmisc bundle packaged for Debian as r-cran-gregmisc ]

 -- Dirk Eddelbuettel <edd@debian.org>  Sat,  3 Sep 2005 20:46:32 -0500


