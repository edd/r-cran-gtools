This is the Debian GNU/Linux r-cran-gtools package of gtools, a
package with miscellaneous R programming functions for GNU R. Gtools
was written and compiled by Gregory Warnes, with code provided by Ben
Bolker and Thomas Lumley, and is maintained by Nitin Jain. Gtools used
to be part of the gregmisc package bundle.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from 
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'gtools' to
'r-cran-gtools' to fit the pattern of CRAN (and non-CRAN) packages for
R.

Copyright (C) 2003 - 2008 Gregory R. Warnes

License: LGPL 2.1

On a Debian GNU/Linux system, the LGPL license is included in the file
/usr/share/common-licenses/LGPL.

For reference, the upstream DESCRIPTION file is included below:

    Package: gtools
    Title: Various R programming tools
    Description: Various R programming tools
    Depends: R (>= 1.9.0)
    Version: 2.1.0
    Date: 2005-08-31
    Author: Gregory R. Warnes. Includes R source code and/or documentation
            contributed by Ben Bolker and Thomas Lumley
    Maintainer: Nitin Jain <nitin.jain@pfizer.com>
    License: LGPL 2.1
    Packaged: Fri Sep  2 19:00:56 2005; jainn02
    
